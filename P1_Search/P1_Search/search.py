# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"
    
    # Initialize the frontier as a stack with the start state and an empty path.
    frontier = [(problem.getStartState(), [])]

    # Initialize the set of explored nodes.
    explored = set()

    while frontier:
        # Pop the node and path from the top of the stack.
        current_state, path = frontier.pop()

        # If the current state is the goal state, return the path to it.
        if problem.isGoalState(current_state):
            return path

        # If the current state has not been explored yet, mark it as explored.
        if current_state not in explored:
            explored.add(current_state)

            # Get the successors of the current state and push them onto the stack.
            successors = problem.getSuccessors(current_state)
            for successor_state, action, _ in successors:
                if successor_state not in explored:
                    frontier.append((successor_state, path + [action]))

    # If no path to the goal is found, return an empty list.
    return []
    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    util.raiseNotDefined()


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first."""

    # Get the start state from the problem.
    start_state = problem.getStartState()

    # Check if the start state is the goal state.
    if problem.isGoalState(start_state):
        return []  # No need to move, as we are already at the goal state.

    visited = set()  # A set to keep track of visited states.
    queue = [(start_state, [])]  # A queue to store states and their corresponding actions.

    # Start the breadth-first search loop.
    while queue:
        current_state, actions = queue.pop(0)  # Dequeue the front of the queue.

        # Check if the current state has not been visited before.
        if current_state not in visited:
            visited.add(current_state)  # Mark the current state as visited.

            # Check if the current state is the goal state.
            if problem.isGoalState(current_state):
                return actions  # Return the list of actions to reach the goal state.

            # Explore successors of the current state.
            for successor, action, cost in problem.getSuccessors(current_state):
                if successor not in visited:
                    new_actions = actions + [action]  # Create a new list of actions.
                    queue.append((successor, new_actions))  # Enqueue the successor with the new actions.

    # If no path to the goal is found, return an empty list.
    return []
    util.raiseNotDefined()  # A placeholder for unimplemented code.



def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""

    # Get the start state from the problem.
    start_state = problem.getStartState()

    # Check if the start state is the goal state.
    if problem.isGoalState(start_state):
        return []  # No need to move, already at the goal state.

    visited = set()  # A set to keep track of visited states.
    priority_queue = util.PriorityQueue()  # A priority queue to store states and their corresponding costs.

    # Push the initial state, an empty list of actions, and a cost of 0 to the priority queue.
    priority_queue.push((start_state, [], 0), 0)

    # Start the Uniform Cost Search loop.
    while not priority_queue.isEmpty():
        current_state, actions, total_cost = priority_queue.pop()  # Get the node with the lowest cost.

        # Check if the current state has not been visited before.
        if current_state not in visited:
            visited.add(current_state)  # Mark the current state as visited.

            # Check if the current state is the goal state.
            if problem.isGoalState(current_state):
                return actions  # Return the list of actions to reach the goal state.

            # Explore successors of the current state.
            for successor, action, step_cost in problem.getSuccessors(current_state):
                if successor not in visited:
                    new_actions = actions + [action]  # Create a new list of actions.
                    new_total_cost = total_cost + step_cost  # Calculate the new total cost.
                    priority_queue.push((successor, new_actions, new_total_cost), new_total_cost)  # Enqueue with the new cost.

    # If no path to the goal is found, return an empty list.
    return []
    util.raiseNotDefined()  # A placeholder for unimplemented code.


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""

    # Get the start state from the problem.
    start_state = problem.getStartState()

    # Check if the start state is the goal state.
    if problem.isGoalState(start_state):
        return []  # No need to move, already at the goal state.

    visited = set()  # A set to keep track of visited states.
    priority_queue = util.PriorityQueue()  # A priority queue to store states and their combined costs.

    # Push the start state with initial values of actions and cost.
    priority_queue.push((start_state, [], 0), 0)

    # Start the A* search loop.
    while not priority_queue.isEmpty():
        current_state, actions, total_cost = priority_queue.pop()  # Get the node with the lowest combined cost.

        # Check if the current state has not been visited before.
        if current_state not in visited:
            visited.add(current_state)  # Mark the current state as visited.

            # Check if the current state is the goal state.
            if problem.isGoalState(current_state):
                return actions  # Return the list of actions to reach the goal state.

            # Explore successors of the current state.
            for successor, action, step_cost in problem.getSuccessors(current_state):
                if successor not in visited:
                    new_actions = actions + [action]  # Create a new list of actions.
                    new_total_cost = total_cost + step_cost  # Calculate the new total cost.

                    # Calculate the heuristic cost from the current state to the goal state.
                    heuristic_cost = heuristic(successor, problem)

                    # Calculate the priority by summing the total cost and heuristic cost.
                    priority = new_total_cost + heuristic_cost

                    # Enqueue the successor state with the new actions and priority.
                    priority_queue.push((successor, new_actions, new_total_cost), priority)

    # If no path to the goal is found, return an empty list.
    return []
    util.raiseNotDefined()  # A placeholder for unimplemented code.



# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
